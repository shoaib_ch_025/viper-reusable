package com.vend.viperreusable.application

import com.vend.viperreusable.dependencies.component.AppComponent
import com.vend.viperreusable.dependencies.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication



/**
 * Created by Shoaib Mushtaq on 19-Oct-18.
 */
class MyApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent : AppComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }

    companion object {
        private lateinit var instance: MyApplication

        fun getInstance(): MyApplication {
            return instance
        }
    }
}