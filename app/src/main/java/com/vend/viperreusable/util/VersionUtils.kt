package com.vend.viperreusable.util

import android.os.Build

/**
 * Created by Shoaib Mushtaq on 06-Nov-18.
 */
object VersionUtils {

    val isMinLollipop: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP

    val isMinM: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

    val isMinKitkat: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

    val isMinJellyBeanMR1: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1
}