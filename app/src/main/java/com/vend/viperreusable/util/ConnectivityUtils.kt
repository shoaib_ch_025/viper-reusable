package com.vend.viperreusable.util

import android.content.Context
import android.net.ConnectivityManager
import com.vend.viperreusable.application.MyApplication

class ConnectivityUtils {
    companion object {
        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun isNetworkAvailable(): Boolean {
            return isNetworkAvailable(MyApplication.getInstance())
        }
    }
}
