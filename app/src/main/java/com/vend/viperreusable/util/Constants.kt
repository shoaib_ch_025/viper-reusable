package com.vend.viperreusable.util

/**
 * Created by Shoaib Mushtaq on 18-Oct-18.
 */
object Constants {
    const val EMPTY_STRING = ""
    //Error Messages
    const val REQUEST_FAILURE = "Something went wrong. Please try again."
    const val NETWORK_ERROR = "Internet not available, check your Internet connectivity and try again."

    object ApiErrorCodes {
        const val INTERNET_NOT_AVAILABLE = 1001
        const val NOT_FOUND = 404
        const val WRONG_CREDENTIALS = 400
        const val MFA_NEEDED = 203
    }
}