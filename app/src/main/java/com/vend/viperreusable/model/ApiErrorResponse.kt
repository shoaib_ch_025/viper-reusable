package com.vend.viperreusable.model

/**
 * Created by Shoaib Mushtaq on 18-Oct-18.
 */
data class ApiErrorResponse(
        val error: String?,
        val errorDescription: String?,
        val errorCode: String?,
        val status: Int,
        val message: String
)
