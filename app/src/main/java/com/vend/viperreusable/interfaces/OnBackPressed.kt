package com.vend.viperreusable.interfaces

/**
 * Created by Shoaib Mushtaq on 13-Nov-18.
 */

interface OnBackPressed {
    fun onBackPressed()
}
