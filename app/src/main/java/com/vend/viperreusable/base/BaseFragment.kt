package com.vend.viperreusable.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

abstract class BaseFragment : Fragment() {
    lateinit var fragmentHelper: FragmentNavigationHelper

    private var v: View? = null

    private var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            fragmentHelper = context as FragmentNavigationHelper
        } catch (e: Exception) {
        }
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater.inflate(getLayoutId(), container, false)
        v!!.isClickable = true
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view, savedInstanceState)
    }

    fun getHelper(): FragmentNavigationHelper {
        return fragmentHelper
    }

    fun getParentView(): View? {
        return this.v
    }

    open fun backButtonVisibility() = View.VISIBLE

    abstract fun getLayoutId(): Int


    open fun initViews(parent: View, savedInstanceState: Bundle?) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideKeyboard()
    }

    fun onKeyDown(): Boolean {
        return false
    }

    protected fun hideKeyboard() {
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(getView()!!.windowToken, 0)
    }

    protected fun showKeyboard() {
        val inputMethodManager = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    protected fun hideKeyboard(input: EditText) {
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(input.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    interface FragmentNavigationHelper {

        fun addFragment(f: BaseFragment, clearBackStack: Boolean, addToBackstack: Boolean)

        fun addFragment(f: BaseFragment, layoutId: Int, clearBackStack: Boolean, addToBackstack: Boolean)

        fun replaceFragment(f: BaseFragment, clearBackStack: Boolean, addToBackstack: Boolean)

        fun replaceFragment(f: BaseFragment, layoutId: Int, clearBackStack: Boolean, addToBackstack: Boolean)

        fun clearFragmentBackStack()

        fun onBack()
    }
}
