package com.vend.viperreusable.base

import android.os.Bundle
import android.support.v4.app.Fragment
import com.vend.viperreusable.R
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


open class BaseFragmentActivity : BaseActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>
    var fragment: BaseFragment? = null

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return supportFragmentInjector
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun getLayoutId() = R.layout.activity_base_fragment

    override fun initViews(savedInstanceState: Bundle?) {
        super.initViews(savedInstanceState)
        val bundle = intent.extras
        if (bundle != null && bundle.containsKey(FRAGMENT_CLASS_NAME)) {
            fragment = Fragment.instantiate(this, bundle.getString(FRAGMENT_CLASS_NAME)) as BaseFragment?
            fragment?.arguments = bundle
            if (savedInstanceState == null) {
                addFragment(fragment!!, true, true)
            }
        }

        supportFragmentManager.addOnBackStackChangedListener {
        }
    }

    companion object {
        const val FRAGMENT_CLASS_NAME = "fragment_class_name"
    }

}
