package com.vend.viperreusable.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.vend.viperreusable.R
import com.vend.viperreusable.interfaces.OnBackPressed

import java.util.*

abstract class BaseActivity : AppCompatActivity(), BaseFragment.FragmentNavigationHelper {

    private var currentFragment: BaseFragment? = null
    private val fragments = Stack<Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = layoutInflater
        val contentView = inflater.inflate(getLayoutId(), null)
        setContentView(contentView)
        initViews(savedInstanceState)
    }

    open fun initViews(savedInstanceState: Bundle?) {

    }

    abstract fun getLayoutId(): Int

    override fun addFragment(f: BaseFragment, clearBackStack: Boolean, addToBackstack: Boolean) {
        addFragment(f, R.id.fragment_container, clearBackStack, addToBackstack)
    }

    override fun addFragment(f: BaseFragment, layoutId: Int, clearBackStack: Boolean, addToBackstack: Boolean) {
        if (clearBackStack) {
            clearFragmentBackStack()
        }

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment_container, f)
        if (addToBackstack) {
            transaction.addToBackStack(null)
        }
        transaction.commitAllowingStateLoss()

        currentFragment = f
        fragments.push(f)

        onFragmentBackStackChanged()
    }

    override fun replaceFragment(f: BaseFragment, clearBackStack: Boolean, addToBackstack: Boolean) {
        replaceFragment(f, R.id.fragment_container, clearBackStack, addToBackstack)
    }

    override fun replaceFragment(f: BaseFragment, layoutId: Int, clearBackStack: Boolean, addToBackstack: Boolean) {
        if (clearBackStack) {
            clearFragmentBackStack()
        }

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(layoutId, f)
        if (addToBackstack) {
            transaction.addToBackStack(null)
        }
        transaction.commitAllowingStateLoss()

        currentFragment = f
        fragments.push(f)

        onFragmentBackStackChanged()
    }

    override fun onBack() {
        for (fragment in supportFragmentManager.fragments) {
            if (fragment is OnBackPressed) {
                fragment.onBackPressed()
                return
            }
        }
        if (supportFragmentManager.backStackEntryCount <= 1) {
            finish()
            return
        }
        supportFragmentManager.popBackStack()
        fragments.pop()
        currentFragment = (if (fragments.isEmpty()) null
        else if (fragments.peek() is BaseFragment) fragments.peek()
        else null) as BaseFragment

        onFragmentBackStackChanged()
    }

    override fun clearFragmentBackStack() {
        val fm = supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }

        if (!fragments.isEmpty()) {
            currentFragment = fragments[0] as BaseFragment
            val homeFragment = fragments[0]
            fragments.clear()
            fragments.push(homeFragment)
        }
    }

    fun onFragmentBackStackChanged() {

    }

    fun getBackStackEntryCount(): Int {
        return supportFragmentManager.backStackEntryCount
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) return if (supportFragmentManager.backStackEntryCount < 1) {
            finish()
            true
        } else {
            onBack()
            true
        }
        return super.onKeyDown(keyCode, event)
    }

    fun hideKeyboard(v: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }

    fun showKeyboard() {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun getCurrentFragment(): BaseFragment? {
        return currentFragment
    }

    fun setCurrentFragment(currentFragment: BaseFragment) {
        this.currentFragment = currentFragment
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        for (fragment in supportFragmentManager.fragments) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}

