package com.vend.viperreusable.dependencies.component

import android.app.Application
import com.vend.viperreusable.application.MyApplication
import com.vend.viperreusable.dependencies.builder.ActivityBuilder
import com.vend.viperreusable.dependencies.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Shoaib Mushtaq on 19-Oct-18.
 */
@Singleton
@Component(modules = [(AppModule::class), (AndroidSupportInjectionModule::class), (ActivityBuilder::class)])
interface AppComponent : AndroidInjector<DaggerApplication> {

    fun inject(myApplication: MyApplication)

    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}
