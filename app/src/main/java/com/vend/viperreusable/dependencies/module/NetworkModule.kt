package com.vend.viperreusable.dependencies.module

import com.vend.viperreusable.network.NetworkService
import com.vend.viperreusable.network.RetrofitClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Shoaib Mushtaq on 16-Oct-18.
 */
@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofitInstance(): Retrofit? {
        return RetrofitClient.retrofitInstance
    }

    @Provides
    @Singleton
    fun provideNetworkService(retrofit: Retrofit): NetworkService {
        return retrofit.create(NetworkService::class.java)
    }
}
