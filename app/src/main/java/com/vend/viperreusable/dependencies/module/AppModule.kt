package com.vend.viperreusable.dependencies.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module


/**
 * Created by Shoaib Mushtaq on 05-Nov-18.
 */
@Module
abstract class AppModule {

    @Binds
    internal abstract fun provideContext(application: Application): Context

}