package com.vend.viperreusable.network

import com.google.gson.Gson
import com.vend.viperreusable.application.MyApplication
import com.vend.viperreusable.model.ApiErrorResponse
import com.vend.viperreusable.util.ConnectivityUtils
import com.vend.viperreusable.util.Constants
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject


/**
 * Created by Muhammad Usman on 5/28/2018.
 */


interface ServiceCallback<T> {
    fun onSuccess(response: T)
    fun onFailure(error: String, statusCode: Int = 0, errorCodeString: String = Constants.EMPTY_STRING)
}

class NetworkHelper @Inject constructor() {

    var disposable: Disposable? = null

    fun <T> serviceCall(call: Single<Response<T>>, callback: ServiceCallback<T>) {
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<Response<T>> {
                    override fun onSuccess(response: Response<T>) = if (response.isSuccessful) {
                        response.body()?.let {
                            callback.onSuccess(it)
                        } ?: run {
                            callback.onFailure(response.message().toString(), response.code())
                        }
                    } else {
                        response.errorBody()?.let {
                            var errorResponse: ApiErrorResponse?
                            try {
                                errorResponse = Gson().fromJson(it.string(), ApiErrorResponse::class.java)

                                errorResponse.errorCode?.let {
                                    callback.onFailure(errorResponse.message, errorResponse.status, errorResponse.errorCode!!)
                                } ?: run {
                                    callback.onFailure(errorResponse.message, errorResponse.status)
                                }
                            } catch (t : Throwable) {
                                callback.onFailure(Constants.REQUEST_FAILURE)
                            }
                        } ?: run {
                            callback.onFailure(Constants.REQUEST_FAILURE)
                        }
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onError(e: Throwable) {
                        e.message?.let {
                            if (!ConnectivityUtils.isNetworkAvailable(MyApplication.getInstance()))
                                callback.onFailure(Constants.NETWORK_ERROR, Constants.ApiErrorCodes.INTERNET_NOT_AVAILABLE)
                            else
                                callback.onFailure(it, Constants.ApiErrorCodes.NOT_FOUND)
                        }
                    }
                })
    }

    fun dispose() {
        disposable?.let {
            if (!it.isDisposed)
                it.dispose()
        }
    }
}
