package com.vend.viperreusable.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Shoaib Mushtaq on 15-Oct-18.
 */
object RetrofitClient {
    private var retrofit: Retrofit? = null
    private val BASE_URL = "https://api.myjson.com/"

    val retrofitInstance: Retrofit?
        get() {

            if (retrofit == null) {
                retrofit = retrofit2.Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
            }
            return retrofit
        }
}
